#!/usr/bin/env python
import sys, pickle, json, os

#One cool hack to make TFClasses visible for pickle
#Needed for transfer functions
import TTH.MEIntegratorStandalone.TFClasses as TFClasses
sys.modules["TFClasses"] = TFClasses

import ROOT
ROOT.gSystem.Load("libFWCoreFWLite")
ROOT.gSystem.Load("libTTHMEIntegratorStandalone")
from ROOT import MEM
from ROOT import TLorentzVector

#Make the C++ classes accessible from python
CvectorPermutations = getattr(ROOT, "std::vector<MEM::Permutations::Permutations>")
CvectorPSVar = getattr(ROOT, "std::vector<MEM::PSVar::PSVar>")

import ConfigParser
import pandas

#https://twiki.cern.ch/twiki/bin/viewauth/CMS/BtagRecommendation94X for DeepCSV
btag_cut = 0.4941

#Define the various hypotheses
INTEGRATION_VARIABLES = {
    "dl_0w2h2t": [],
    "sl_2w2h2t": [],
    "sl_1w2h2t": [
        ROOT.MEM.PSVar.cos_q1, ROOT.MEM.PSVar.phi_q1
    ],
    "sl_0w2h2t": [
        ROOT.MEM.PSVar.cos_q1, ROOT.MEM.PSVar.phi_q1, ROOT.MEM.PSVar.cos_qbar1, ROOT.MEM.PSVar.phi_qbar1
    ],
    "fh_4w2h2t": []
}

#Adds a transfer function to the jet
def add_tf(jet_eta, obj, tf_matrix):
    jet_eta_bin = 0
    if abs(jet_eta)>1.0:
        jet_eta_bin = 1
    tf_b = tf_matrix['b'][jet_eta_bin].Make_Formula(False)
    tf_l = tf_matrix['l'][jet_eta_bin].Make_Formula(False)
    
    #print "jet_eta", jet_eta
    #print "TF b"
    #tf_b.Print()
    #for ip in range(tf_b.GetNpar()):
    #    print ip, tf_b.GetParameter(ip)
    #print "TF l"
    #tf_l.Print()

#    tf_b.SetNpx(10000)
#    tf_b.SetRange(0,2000)
#
#    tf_l.SetNpx(10000)
#    tf_l.SetRange(0,2000)

    obj.addTransferFunction(MEM.TFType.bReco, tf_b)
    obj.addTransferFunction(MEM.TFType.qReco, tf_l)

#Given the dataframe line, parse jets
def parse_jet(line, tf_matrix):
    num_jets = min(int(line["num_jets"]), 9)
    objects = []
    for ijet in range(num_jets):
        s = "jets_{0}_{1}"
        pt, eta, phi, m, btag = line[s.format("pt", ijet)], line[s.format("eta", ijet)], line[s.format("phi", ijet)], line[s.format("mass", ijet)], line[s.format("btagDeepCSV", ijet)]
        btag = btag > btag_cut
        v = TLorentzVector()
        v.SetPtEtaPhiM(float(pt), float(eta), float(phi), float(m))
        o = MEM.Object(v, MEM.ObjectType.Jet)
        o.addObs(MEM.Observable.BTAG, float(btag))
        add_tf(float(eta), o, tf_matrix)
        objects += [o]
    return objects

#Given an ascii line, creates a lepton
def parse_lep(line):
    num_leptons = int(line["num_leptons"])
    objects = []
    for ilep in range(num_leptons):
        s = "leptons_{0}_{1}"
        pt, eta, phi, m, charge = line[s.format("pt", ilep)], line[s.format("eta", ilep)], line[s.format("phi", ilep)], line[s.format("mass", ilep)], -1.0
        v = TLorentzVector()
        v.SetPtEtaPhiM(float(pt), float(eta), float(phi), float(m))
        o = MEM.Object(v, MEM.ObjectType.Lepton)
        o.addObs(MEM.Observable.CHARGE, float(charge))
        objects += [o]
    return objects

def parse_met(line):
    pt, phi = line["met_pt"], line["met_phi"]
    
    v = TLorentzVector()
    v.SetPtEtaPhiM(float(pt), 0, float(phi), 0)
    o = MEM.Object(v, MEM.ObjectType.MET)
    return o

#Set the global transfer functions used for jet efficiency computations
def set_tf_global(cfg, tf_matrix):
    for nb in [0, 1]:
        for fl1, fl2 in [('b', MEM.TFType.bLost), ('l', MEM.TFType.qLost)]:
            tf = tf_matrix[fl1][nb].Make_CDF()
            #print "CDF", nb, fl1
            #tf.Print()
            #for ip in range(tf.GetNpar()):
            #    print ip, tf.GetParameter(ip)
            #set pt cut for efficiency function
            tf.SetParameter(0, 30)
            #tf.SetNpx(10000)
            #tf.SetRange(0,2000)
            cfg.set_tf_global(fl2, nb, tf)

#Run the integrator on a configuration file
def integrate_dataframe_line(line, tf_matrix):
    

    objs = []
    jets = parse_jet(line, tf_matrix)
    leps = parse_lep(line)
    met = parse_met(line)
    objs = jets + leps + [met]

    if (len(jets) == 6 and len(filter(lambda x: x.getObs(MEM.Observable.BTAG)==1, jets)) == 4 and len(leps) == 1):
        integration_hypo = "sl_2w2h2t"
        fstate = ROOT.MEM.FinalState.LH
    elif (len(jets) == 4 and len(filter(lambda x: x.getObs(MEM.Observable.BTAG)==1, jets)) == 4 and len(leps) == 2): 
        integration_hypo = "dl_0w2h2t"
        fstate = ROOT.MEM.FinalState.LL
    else:
        return False

    cfg = MEM.MEMConfig()
    cfg.defaultCfg(1)
    strat = CvectorPermutations()
    strat.push_back(MEM.Permutations.QQbarBBbarSymmetry)
    strat.push_back(MEM.Permutations.QUntagged)
    strat.push_back(MEM.Permutations.BTagged)
    cfg.perm_pruning = strat
    set_tf_global(cfg, tf_matrix)
    cfg.transfer_function_method = MEM.TFMethod.External
    cfg.integrator_type = MEM.IntegratorType.Vegas
    
    integration_variables = INTEGRATION_VARIABLES[integration_hypo]
    
    vars_to_integrate = CvectorPSVar()
    vars_to_marginalize = CvectorPSVar()
    for v in integration_variables:
        vars_to_integrate.push_back(v)

    mem = MEM.Integrand(0, cfg)
    for obj in objs:
        mem.push_back_object(obj)
    ret1 = mem.run(fstate, ROOT.MEM.Hypothesis.TTH, vars_to_integrate, vars_to_marginalize)
    ret2 = mem.run(fstate, ROOT.MEM.Hypothesis.TTBB, vars_to_integrate, vars_to_marginalize)
    
    return {"mem_tth": ret1.p, "mem_ttbb": ret2.p, "mem_ratio": ret1.p/(ret1.p + 0.1*ret2.p) if ret1.p > 0.0 else 0.0}

if __name__ == "__main__":

    #Load the transfers
    try:
        pi_file = open(os.environ["CMSSW_BASE"] + "/src/TTH/MEIntegratorStandalone/data/transfers.pickle", 'rb')
        tf_matrix = pickle.load(pi_file)
    except Exception as e:
        print "Could not load transfer functions", e
        tf_matrix = None

    fns = os.environ["FILE_NAMES"].split()
    if len(fns) != 1:
        raise Exception("Supply exactly one file name in $FILE_NAMES, found {0}".format(len(fns)))
    df = pandas.read_csv(fns[0], delim_whitespace=True)
    odf = pandas.DataFrame()
    out_data = []
    for iline, line in df.iterrows():
        ret = {"mem_tth": 0.0, "mem_ttbb": 0.0, "mem_ratio": 0.0}
        ret_int = integrate_dataframe_line(line, tf_matrix)
        if ret_int:
            ret = ret_int
        out_data += [ret]
    odf = pandas.DataFrame(out_data)
    odf.to_csv("out.csv")
